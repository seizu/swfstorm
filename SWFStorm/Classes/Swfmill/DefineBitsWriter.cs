﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SWFStorm.Swf
{
    public class DefineBitsWriter : DefineBitsReader
    {
        public DefineBitsWriter(string xml) : base(xml)
        {            
        }

        private new DefineBits Read()
        {
            return null;
        }

        public void WriteString(string objectID, string data)
        {
            var targetElements = (from x in Xml.Descendants()
                              where supportTags.Contains(x.Name.ToString())
                              select x);

            var elm = (from x in targetElements where x.Attribute("objectID").Value == objectID select x).FirstOrDefault();
            elm.Element("data").Element("data").SetValue(data);
        }
    }
}
