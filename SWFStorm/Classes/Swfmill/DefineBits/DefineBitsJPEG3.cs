﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Net;
using System.IO;
using System.IO.Compression;

using System.Xml.Linq;

namespace SWFStorm.Swf
{
    // DefineBitsJPEG2 のデータに alpha channel (透明度)データが加わったもの
    // http://pwiki.awm.jp/~yoya/?Flash/SWF/format/Jpeg#tag35
    // sample
    //<DefineBitsJPEG3 objectID="47" offset_to_alpha="3086">
    //    <data>
    //      <data>/9j/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2f/Y/+AAEEpGSUYAAQEAAAEAAQAA/9sAQwAGBAUGBQQGBgUGBwcGCAoQCgoJCQoUDg8MEBcUGBgXFBYWGh0lHxobIxwWFiAsICMmJykqKRkfLTAtKDAlKCko/9sAQwEHBwcKCAoTCgoTKBoWGigoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgo/8AAEQgAhAFBAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A+VKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigCSGJpn2rVsaa+Pvj8qdpBG6T14rTqWy4xTRlf2a/98flQ2nOBwwP4Vq0UXZXKjAlheI/MKiroZY1kUhhxWHcxeVKV7dqadyJRsRU+KNpXCr1plaelRcFz+FDEldjf7NOP9Zz9KpTRNC+1q6Cs7Vk4Rx9DSTLlFJGcqlmAHU1eXTWK5L4P0qLTU3T5PatmhsUY33Mo6a/Zx+VH9nP/fH5Vq0UrsrlRlf2c/8AfH5UDTX7uPyrVoouw5UZL6c4I2sD+FP/ALNOP9Zz9K06KLsOVGUdNf8Avj8qP7Of++PyrVoouw5UZX9nP/fH5Uo01u7j8q1KKLsOVGSdOk3YDDHrTv7Nf++PyrUop3YcqMr+zX/vj8qr3Fs8H3uR61u1U1Ir5Bz1oTE4qxkIpdgqjJNXV05yMlgPwqPTSBcc1s0NijFMyv7Nf++Pyo/s5/74/KtWildlcqMv+zWx98Z+lUpYzG5Vuoroax9Tx5/HpTTJlFJFOiiiqICiiigAooooAKKKKACiiigB8UjRuCpwa3oWLRqT1Irnx1rft/8AUr9KllwJKKKKRoFZWqgeYv0rVrH1IN52WHHamiZ7FRRlgB3retk8uFV9qytPi8yfJ6LzW1QxQXUKhu4/MhYVNQeRSLMjTXEcxVuM1rZGM5rGvojFOSOh5qIzyFcbzina5mpW0NhLhXmKL1FT1kaX/rz9K16TKi7oRjgE1HBMswO3tT5PuH6VhLK8UjFDjmhK4N2N8kCkZgoyTxWE9xK5yWNNaaRhhmOKdhc5tW9wszMF/hqas3SOsn4VpUmUndDJXEaFj0FEMiyoGU1He/8AHu30rHinkiBCNgU0ricrM2pZAhUZ5JqUdKw4ZGkuULnPNbg6UmrAncD0rDvJGeZgx4BrcPSsC5/17/WmhTI1JUgg4NbVhI0kILdaxK2NM/496bJhuXKKKKk1AnArDvnD3DEdK22GQQawrtBHOwHSmiJkNFFFUZhRRRQAUUUUAFFFFABRRRQAo61v2/8AqV+lYA61v2/+pX6VMi4ElFFFI0CoLyISQkY5FT0EZGKAKGlR7Udj1Jq/TY0CLgU40CSsgoqBbqNpNgPNT0Bcp6lFvh3DqKx66J13KQe9YM6GOVlPrTRE11LOl/68/StesjS/9efpWvQ9yobDZPuH6Vz7/fb610En3D9K59/vt9aETMbRRRVEGlpHWT8K0qzdI6yfhWlUPc1jsQXv/Hu30rCrdvf+Pd/pWFVIme5Na/8AHwn1reHSsG1/4+E+tbw6UmOGwHpWBc/69/rW+elYphMt2y44zQgmVa2NM/496VbGIJgjn1qa3iEKbR0obCMWmS0UUUiwPSsG8Obh63iMisW/gMUmc5BpoiexVoooqjMKKKKACiiigAooooAKKKKALFlB58mCcAc1tooVQB0FYtjMIZct0NawuIyM7hUs0haxLRUfnx/3hQZox1YUirklFU5r6NPu/Makhu45FzuAPoaLBdFikcZUimefH/eH502S5jVSdwoC5kSAw3J5yQc1tRNvjU+orCnk8yUt61pWV0hjCucEU2RF6l6oJ7WOXJYc+tP8+P8AvD86a9zGqk7hSLdiK0tBAzNnJNW6qW94khYMcY6VP58f94UArdCQjIxWVqNsseHTv1FaPnx/3h+dZ+pXCuFRDn1poUrWM+lAycUlKDg5qjI2rG3EMec5Ldas1UtbpHjG4gEetT+fH/eFQbK1h7qHUqehrHv7cQOpU8NWqZ4/7wrL1GdZnUL0WmiZWsSadbBsSMenQVqVl6fcqi7HOPStDz4/7woY42sSUgVQcgDNM8+P+8KXzk/vCkMfRUT3EaDJYUiXMbDIYUBcmoqPz4/7woE0Z/iFA7klZ2rHhRVxp41GSwrIvZ/OlyPujpTRMnoV6KKKoyCiiigAooooAKKKKACiiigApaSigBcmjJ9aSigAooooAXNFJRQAUUUUALmikooAKXNJRQAuaSiigAooooAKXNJRQAuaSiigApc0lFAC5oyfU0lFAC5NFJRQAuaMn1NJRQAuT60lFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAH/9l42u1ceVgT1xZnJishsskiO5SKCGppKWpbRa22uBWrtSpPi2itqK361FptFfdqbYtateBaLVapWlyxKqAsYmsVEWQRQWSPrEkI2WfhzSQBQjJJFKfR933z+yfzZc7ce+5v7j333HPPHQsLChQoUKBAgQIFChQoUKBAgQIFChQoUKBAgQIFChQoUKBAgQIFChQoUKBAgQIFChT+r0BjMJkMGqB/A6QbuKEFAAABtQSAl0MHjdcFgCDw/BoDZBSi31o2xwoDx5LFYllqLk01HwPd2q1vQKCfpx1LR5Rm5ezbP6Cfl0MvK0umAVhy7Zxdne0saRYA087TL6C/r4stl8NmEUuz2DYOLi4OvZggANJZ3bXlsJk03RYx2BqRbqWwrWyd3Zx6MUnlkG4/IHzJtz/FxcfH7YndsnFL7B78cu/O1Qs+HhnQx4pu8FVy+k79/uKtvPyclP0LQuy0msB0D4s5dfNefk76H4d/jt2wnhAbYvcnJqdcStgU8XbovLiUnPy8vy/9tu+n7zYRi2/afjTpytWkPUsnjpqyePOun+NwFbdu2rpzL6b296tmvuXK7mAFYPV5c/rK7epWbN2oVcjGbXuOXUw9s2fRO84M8gh0Dl2ekFVUXllVVfm4tLiguPQxfllRdjfjbPzq6UM8rGjEzzG9Zxy61yiWyST8sgsrQqy7up9z2A/ZdSLsRmtDTUVpgQGUVdfzhfza3NPf/3C+hC+RycQtvKrykkJi6cKSuiaBsLEs49f4k1kFjyoqcRUfFD4oq8DULrlz7tvJvpYaBlleEzacvlWibsUD7fIKHzzmtQgbSy9veNeJRtrwDd2QVtkqV0IYlAq5XK5QXUJKqajx8e2kbbOCHZmEzzmO3VsgglEMiKz6woJ+nXe4IetuNitVN2B1kYRQQjCCIpC4rqCgTgqbFFdAmAysEFQ/5mm0xYVVyioV4vqcfdO81XrSXcN/+psnVkCdLdIqRInVCcvrM2OG9CJrGFsuT6tXIGi7Glgz2jsvYaW4Pj9pzfuebILKLAetzhbAamlUVnZwfMcUQHefdZan7CwGL5IQmruIUipRIialNfKYuBLS0xZFodb8XWPscTUB63e23uZDneUTFaLknY/yopPDH+CQUKnoam13YIpJ6rJ3TvGx1GMQdAg/Vt35ICK8sYLVwWzw5jyJoRIJK0GfQbqdWBiFW6598Spu2Ohecy81QMZLRKX3vw22JGkK8c4QIsbUVfJz46d6s/QsoN+y7NbOB1HF4wO2Go/EbsKxWuhZKCEFqLwkdigHq58dtOm+1FT1UM0v79mQQyDDP9d4dSgkzIub5Kbb4a1H7C3T6rkIP9VbLUJzn5siQNrNDrguYawtVr9VaFyF0qRww8lwe3KMIDMgX26iv8PCnO0j7LpXB7rNutgEawnJCgdz1AX2X50jQV8AgRpOrEf/UgOZEkYaT08iiUCTPRDvg02pi/27D2L2wLV3u9EE8T5yUk0jnCE7ShUvgsAnx8fjVoQ7Yl+V6R5YnzjBlhwC6T5ZIsSkfSk/NK63dn2AbdjhKqU2TYjoS3+VG2ET9mstZH7+UMWjPcO5+KsN/q5YbtIGVh98txdJbqBjYq3SVH2IMHNJX23fne41L4WPdG9A3EjclwacZlxoNr8JxOxM9pf98TfI8F10rQU2YZUUpbGDSZqFLThrb7aYmjRReemud7jaTmDwtwU6Ix/+M8od8+5pXgszReafg2Hxg/0THHEbAtiP+Ql38I23p3DzIBZZ8ZSxO241yEy8Mrj+9BSHrjEM9A4/XqczTtG8Ta9jL5Xh91WO9F8lEEUQRKcCVCl88NvsV1maJWbELwV8hbFRgMry1gYwyVrLeUzYcrm8DTY+hluvz/foWj0yfL/IbNXREK1JmIjZSWZATJ4M/Vc7m0wsh3X4a/pnX2SAlcYR5fhF7M6qlRkrQ3JnZV86WQSyXIYtPl1pfOJEJbe/1KrR6u3YB3qGujVjMbYUYAas1SYQW9/Cuv2F+E/NDUjfnGD/IlorTNmTiqbuVhsV39sxydcK7AwTeYetTWkw1hxR1iIv0qIJFnS7kHUmXDe8z/fv7POg87Sker0uK3+4E7OTjL4r/hF3tVYh5DWKdewDCkuam9qURJ1L0lRbp/sqsTIa+V02BhHez37U3Ugg/MtzX9EKSQJM5zG7S400COH/OasPSGJE0HvRjTYTVvf+ukBml/P91S19ebjhbIQLjeb2SXKnh43K67ITr5Tr2ERU+vjapdxm/Z4GtxZdPHqyWce4Qs05yekVHWWgUF1qUr64O4HNZz5y6MYH+7VNhagRx7Y2YZwdiUFVmkd0likC82O6CLQeFf9Yf8ij4jvfDGAB1iN2FUkQjW2qT4mZsiZDZ2WHCDI3LNydi+gEE7CZtOjQp+Nnl3cvGlWUH1m86nytpsci4vzD+2636RCYNLmbm2rBDFxfYLhBSNvdjW9YWpiVQGnu1/06HEGae+SlZv1JB1VWHg6zBRg+s09XSPD4HiJvyFw/3CXinI5fiDSfnxU8PUGqMoMoLJdIlZiNQ6C2h8dm+TmFFHa3rpjLsTV0+JrsFpU0Iqs8tXLzzeciEJVXHJ/mRjMvgW03l/jQOsfH+lwik4nwU+d70wFu0OLThQ0iibjlUcqm0U7MyUl6BCZNcQ9cVC5SwjAsbym9e79aIJa08nKPzulvxR5QoEtgwfrX3MN2/CODYFgprry4fOLyrOciEG6+tnwQx8KsBGJmOtJFY2UAu7FHqokWL6iscFsIxwK0DZode/bG7b8u7Vs83JkBEhE42dE2+Oit8icNdSVpcWu3/Jpy65/037d87M8FmYEEBA7gen6wtbC2ob4y58SSoYMWPReBqLLmxFRXmnkJhOqOje+wunTv+WnE8SqI9/tkbDUAcr2HTf/8yyWfjPa3o1sAhAT2ptt8uPrn4ycTYhe9Nyh4/NxlK6Inh7hZYn4kEYGBLJbrsO+OnEzcHzNtYG+f6OcjEKpPnt+XaVYCUVnRdyEdnZ4Tsq2Q2FVGRNnL/TDNAEYvZ+++r7jY4NuHBggEQOfA0AnhY4f62rM59u6+r3o6cGiqphMRyLSg24S8/8HEUYNcOUyP5yOwHZEU//x+b9CcBCKCawt8Ovxoh0mJPMjAGr1s70jrjp1ymlpFQwRagCyurb0th4GTDNLpml1oQwRaAJY29na9WDQCbZ91EoH5GUv9mOYkUFl1eFznKxu286GhcBEqyV0fxNbZdDFEoIEAryECjWj7rG5MO8xTxw7NRiDceHnxQK66RjDibANs0D5X/zpBh5uXkUASA9JPaQOl5Ylz/NSuJ+vr22LD5kWY/rkv4+XvgY2nws1LICwpOTLNE28GYLO/QmnER30Q+zb3/2AIHxtrY15HGhbl7RyDrzdpHpdbYGOBwzMzXMCXnUBlVXwo16wE4hvXmSsHWeLbrnnG4qWo+Da2IH7JCUQVpT+EsM1LIFZp9fGpLjTAfny1sS0UzYL4JSdQXriJtIj+0xLYjrTdWfc6h+GzUIgYX/ThC+KXnEBSI/pPSyCqrD3xoVOvwdtlJkKvhd8N5rzkBJIa0X9aArGF2s3l/m6Tfzex6QvxTmq22F9eAttufuFNMzuB+N7myDeWZSOmeP5Ls8X+8hLYem2eO2h2AtvhJ7//Z9KeUpOzzaO4kTYvjkBV4rpxAuHmc9OdAPMTiAhSV6240GiS5+Y/ozzoL4ZAkG3jhCeucwYYCyYoKw+MJtOPfloCUXHOvgO5YpNikrzNWtmLZiQQYDsPGDN99sxxwa8M2WSEQGyeG8J9AT0QlT9Ky6ozmfmEQnXHP3QEzU8gyPUL//rg+ZRLCZujZu4qNkJgSdwHHmzA/ARCLeVVbdpkoCgMQXjyt04aQ/ayrnCb2QgErAKiDv1d2Szk1947u/98hZEh3JT9wweeTLMT2I7I2yTaiQMoLG2qqappksKogbiqGQkEmT6zfisRKfHMf0ld0UO+sXmu6faOMEfQ7ASqciy675BfPbLviN7OOdycHOlOI4/ADQOegkCa3ZjdReokCBRRSsRK42PpxupBlmYnkCDO+uW4EeOWX2qAdKaRe10b10QEnvvY8RkILP72NZYJAs9FeNj2/yKND3cenkBQEyv7Yx/0Bl4wgai8ZOdIR65j6I/F3Zd3KFR7YooLAzBEoODyJy7g0xLYriyLHcIxTqAgY82E0ZEHS+VP3QhEmL6AvPQiuvfnN3pAINKahedjMXwWXtNN3hD9s3GUj3qbDZx6VodAVHRjqT/HAIPs13QyE9qhqkNjnRiAEW1RafmVXw7+kSuAn/7lS++u7kfWeTnQ+s11PUmsh+tPTnYALAD78Ud1QlyokpeyfcGH+EYvwFmYqkMvKiuOmzbQgXAA0R1Hl+mk3cBNKV+N8uaChrVFYXFjTfWTViX6DKNHO9nnOQew3esLTlX1ILEeqjo4xgYgTPVAZfX30xO3fOxvY93vQJHuTag5J+Gr94kimgzn4asbdFNfpRVXd8wOsjOirSqvEEKe5XiUPH8dWQRaBy89UybuQV44VJf4kQueFu0WmaybbIRCUjzZ5dM3g+fcF8B6GbmtVVk73fVHEM3x3Q2puuEyFJY1FZ1eGqzqsUCPtdV1pu9905+cIQy8F/9Q2iOFsIF6ZiburjD9V94i2KnDRlZZ6tUSwvRrrBcu1tvbprnPTCJe6SDSh/F4aglg12NtdXNUM6I9yZlE6NFp/B5qhLsr+D466Bp1lShbBmOwod7A7ITKdw/j6k0gG+4ZsMUIPxVvL83zs1Q+GYcoVDmq5LgxrA35PU0KRzEriJ9XAfrMukyYboQtCpSGZkb4pN7edq+R+ysNTASo9B4ehGcGrL1HyhkAuOEUWVvrljvKlD1Woz5xIjaysB6YIjTQcMN78MkRzjo5DLbjjz+BDXm+D97AvGnWG9sekHKOTPPqSSHwx54fbYOfHMePnIEun1w2cEDTCIEX9IKaNmOP8QwSWByEExi0tZgMAlF50bZgkjY2mTGmBgV+KoH4lLOyIn4EFz8cFP47YddBYYXC0BCGfpugm+fNHf7zY0NDWHLHn4EfjVx1h4yjoIgw8wtfkraVaHOvNBs7p4QicmGjQI4Q73SuHYgvU62G/lAsI7KRrTU1hsa29Pshumm2rAFr7rQhxPv6TRfdNB5TE2x6+8bktkPlrxMdSFoKA6N2FYoN6YSisFzwKPPCjRqZvpuKyCpPTFPlyjJ85iXz9M/TQIL8M0l5hAfxMBdorrduF6C5fny8UkbIX1v+j6qjjNahOwrEpo4SIkoljBrd/W+6vmIAaakJHWE0FRBE6wsNCKwQN5WmH1g2e21yhRhCun2+AYEkVclLBnLU7u3QtWli/IsYWoAVLbn7PpsXz1cgut+PQCApL32ItV4X4AxcklwF6YsrhcVHZ6jcXobXf451adtd3c56BdUVjVLIoAQsb/hr2yjyUlStAuYcvl3dIhK1tYmEfIEQ+xVL5XK5uLWFV3b7wu7FYf5eQ5ecyK3l4yKYjLAVFxXwCv5YNthWrQbdKfSrO48b+PgdNUTCxkfpOz7y9/8oq6JZ9beoVai5LRLUF12IcdS3QaDt4GV/8AQqaaweVX3Yc82Vf+2P9FevRDj9ow7dqmrRqMJvUQmrFcYhk4qayjISDibf5+FKigQtfKFGQqaREDWUXNky1pW8L++A3H6TY45cup6ZlZmecunPtIzMG7dy8+/n/339wrGdq2aG9rVnMWwHTt+YgItkZWWmplzLyMxMv5K4PTKo83tF9N5B38Sd/PNaRpYamRmpZ+P+O8bT0tJz5YHz+N+Z6WkpaemqEjJSTv346RAG4bo8KDLxCi6VcT0l5bqqtMzrFw+tnviqVYe2fpPWqLTF1U2+ePU6JqJSGP+yzv38nMxze5eGh0VtO4GVknn96sXkFFWlf9/tkMhI2rVgmAuZmR0Au0/Q2JnzoqOjP4uaOXPu/OhFy76OWR/z33kzxr3l72qNR5JAjtsb41Qi0dFzZn86HxONDB/ixe0aBqDVgBGTZn4a3YH5c6aO9LdnAgAzcPT0uap/5s7GylZdRX34lo+1gbfpFR75GS40LypKXV/0vIj3Bjp1fdiJ7fxamFqVz6IiZkTiVyqFVYhZtWj6iH6Odl6Dw/FS5kXOiIjCLxYuXdUhsWDqsFdtyEzswF8827aPu4eHh7urcx83Dw8vX//AwMBX3J1sOcwOimiWdioRDw9XVzdc1MXeitHNiLG49vjDHXBz7KV+mG3jpPrb3c3VTV2Cu6uDzrNab5Nh7+KuFnJVS3u4O9uwQQJtMREnJ5WwWmEcAX5ezvgHyugce1WDXJycVMV4vdIvQCPh5chlkP8BNwCg4QABAMR+6Awmk0nT+U6cRoQGgqBalKAQkNYJrfuavzUPEj9roStN65TW/4pcpyodNaoUVqHzm3NAdwlapwTtX/j8HQUKFChQoECBAgUKFChQoECBAgUKFChQoECBAgUKFChQoECBAgUKFChQoECBAgUKT4n/AQe8QhM=</data>
    //    </data>
    //  </DefineBitsJPEG3>
    class DefineBitsJPEG3 : DefineBitsJPEG2
    {
        protected int offsetToAlpha;

        public DefineBitsJPEG3(XElement elm) : base (elm)
        {
        }

        protected override void initialize()
        {
            var elm = this.xelement;
            offsetToAlpha = Convert.ToInt32(this.xelement.Attribute("offset_to_alpha").Value);
            Bitmap = decodeImage();
            ObjectID = this.xelement.Attribute("objectID").Value;
            ExtraInfo = String.Format("offset_to_alpha={0}", offsetToAlpha);
        }

        protected override Bitmap decodeImage()
        {
            var jpeg = base.decodeImage();
            var bmp = new Bitmap(jpeg.Width, jpeg.Height);
            var data = this.xelement.Element("data").Element("data").Value;
            var src = Convert.FromBase64String(data);

            using (var ms1 = new MemoryStream())
            {
                ms1.Write(this.gzMagicHeader, 0, this.gzMagicHeader.Length);
                using (var ms2 = new MemoryStream(src))
                {
                    ms2.Seek(offsetToAlpha, SeekOrigin.Begin);
                    var br = new BinaryReader(ms2);
                    var buffer = br.ReadBytes((int)(ms2.Length - offsetToAlpha));
                    ms1.Write(buffer, 0, buffer.Length);
                }

                ms1.Seek(0, SeekOrigin.Begin);
                var gs = new GZipStream(ms1, CompressionMode.Decompress);
                var br1 = new BinaryReader(gs);
                
                for (var iy = 0; iy < jpeg.Height; iy++)
                {
                    for (var ix = 0; ix < jpeg.Width; ix++)
                    {
                        var a = br1.ReadByte();
                        var c = jpeg.GetPixel(ix, iy);
                        bmp.SetPixel(ix, iy, Color.FromArgb(a, c.R, c.G, c.B));
                    }
                }
            }

            return bmp;
        }
    }
}
