﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using SWFStorm.Jpeg;

namespace SWFStorm.Swf
{
    /// <summary>
    /// 量子化テーブル（DQT）とハフマンテーブル（DHT）からなる共用JPEGテーブル。
    /// </summary>
    public class JPEGTables
    {
        protected IEnumerable<JPEGSegment> segments;
        public IEnumerable<JPEGSegment> DQTs { get; protected set; }
        public IEnumerable<JPEGSegment> DHTs { get; protected set; }

        public JPEGTables(XElement xelement)
        {
            var data = xelement.Element("data").Value;
            var bytes = Convert.FromBase64String(data);
            using (var ms = new MemoryStream(bytes))
            {
                var sr = new JPEGSegmentReader(ms);
                segments = sr.ReadImage();
            }

            checkSegments();
        }

        public JPEGTables(IEnumerable<JPEGSegment> jpegSegments)
        {
            segments = jpegSegments;
            checkSegments();
        }

        protected void checkSegments()
        {
            if (segments.Count() < 4)
            {
                throw new SwfmillException(Resources.MessagesResource.JPEGTablesSegmentsInvalidLength);
            }

            fetchDQT();
            fetchDHT();
        }

        protected void fetchDQT()
        {
            DQTs = from x in segments where x.Marker == JPEGSegmentMarker.DQT select x;
            if (DQTs.Count() == 0)
            {
                throw new SwfmillException(Resources.MessagesResource.JPEGTablesNotFoundDQTSegment);
            }
        }

        protected void fetchDHT()
        {
            DHTs = from x in segments where x.Marker == JPEGSegmentMarker.DHT select x;
            if (DHTs.Count() == 0)
            {
                throw new SwfmillException(Resources.MessagesResource.JPEGTablesNotFoundDHTSegment);
            }
        }
    }
}
