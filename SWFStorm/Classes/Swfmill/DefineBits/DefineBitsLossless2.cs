﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Xml.Linq;
using System.IO;
using System.IO.Compression;

namespace SWFStorm.Swf
{
    /// <summary>
    /// 透過色を含むロスレスフォーマット（PNG/GIF）
    /// </summary>
    class DefineBitsLossless2 : DefineBitsLossless
    {
        public DefineBitsLossless2(XElement elm) : base(elm)
        {
        }

        /// <summary>
        /// Format=3において唯一DefineBitsLosslessと処理が異なる部分。
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        protected override Color[] readColorMap(Stream stream)
        {
            var br = new BinaryReader(stream);
            var colormap = new System.Drawing.Color[nColorMap + 1];
            for (var i = 0; i < nColorMap + 1; i++)
            {
                colormap[i] = System.Drawing.Color.FromArgb(br.ReadInt32());
            }

            return colormap;
        }
    }
}
