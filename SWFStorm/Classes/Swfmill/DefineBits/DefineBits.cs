﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Xml.Linq;
using System.IO;

using SWFStorm.Jpeg;

namespace SWFStorm.Swf
{


    /// <summary>
    /// 
    /// </summary>
    public class DefineBits
    {
        protected XElement xelement;
        public string ObjectID { get; protected set; }
        public Bitmap Bitmap { get; protected set; }
        public string Type { get; protected set; }
        public JPEGTables JpegTables { get; protected set; }
        public string ExtraInfo { get; protected set; }

        //XXX:マジックヘッダ。なんで入れるんだろう
        protected byte[] gzMagicHeader = new byte[] {
            0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00
        };

        protected virtual Bitmap decodeImage()
        {
            var data = this.xelement.Element("data").Element("data").Value;
            var src = Convert.FromBase64String(data);
            using (var ms = new MemoryStream(src))
            {
                var sr = new JPEGSegmentReader(ms);
                var defineBits = sr.ReadImage();
                var bmp = convertToBitmap(JpegTables, defineBits);
                return bmp;
            }
        }

        public DefineBits(XElement elm, JPEGTables jpegTables = null)
        {
            xelement = elm;
            Type = elm.Name.ToString();
            JpegTables = jpegTables;
            initialize();
        }

        protected virtual void initialize()
        {
            ObjectID = xelement.Attribute("objectID").Value;
            Bitmap = decodeImage();
            ExtraInfo = "";
        }

        public virtual Dictionary<string, string> GetExtendedInfo()
        {
            return new Dictionary<string, string>();
        }

        protected Bitmap convertToBitmap(JPEGTables jpegTables, List<JPEGSegment> defineBits)
        {
            using (var ms = new MemoryStream())
            {
                writeSegment(ms, new JPEGSegment(JPEGSegmentMarker.SOI, null, 0));

                var segmentsList = new List<IEnumerable<JPEGSegment>>();
                segmentsList.Add(from x in defineBits where x.Marker == JPEGSegmentMarker.APP0 select x);
                segmentsList.Add(jpegTables.DQTs);
                segmentsList.Add(from x in defineBits where x.Marker == JPEGSegmentMarker.SOF0 select x);
                segmentsList.Add(jpegTables.DHTs);
                segmentsList.Add(from x in defineBits where x.Marker == JPEGSegmentMarker.SOS select x);

                foreach (var segments in segmentsList)
                {
                    foreach (var segment in segments)
                    {
                        writeSegment(ms, segment);
                    }
                }

                writeSegment(ms, new JPEGSegment(JPEGSegmentMarker.EOI, null, 0));
                return new Bitmap(ms);
            }
        }

        /// <summary>
        /// JPEGセグメントの書き込み。SegmentWriter必要だよね。
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="segment"></param>
        protected void writeSegment(Stream stream, JPEGSegment segment)
        {
            var marker = new byte[] { 0xFF, (byte)segment.Marker };
            stream.Write(marker, 0, marker.Length);

            if (segment.Marker != JPEGSegmentMarker.SOI && segment.Marker != JPEGSegmentMarker.EOI && segment.Marker != JPEGSegmentMarker.SOS)
            {
                var lb = BitConverter.GetBytes((ushort)segment.Length);
                stream.Write(new byte[] { lb[1], lb[0] }, 0, lb.Length);
            }

            if (segment.Data != null)
            {
                stream.Write(segment.Data, 0, segment.Data.Length);
            }
        }
    }
}
