﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.IO;
using System.IO.Compression;
using System.Drawing;
using System.Net;

using SWFStorm.Jpeg;

namespace SWFStorm.Swf
{
    // JPEG データの圧縮テーブルに関するセグメントが encoding_tables (又は、別の tag である JPEGTables の方)に、それ以外のセグメントが image_data に入る。
    // http://pwiki.awm.jp/~yoya/?Flash/SWF/format/Jpeg#tag21
    // http://labs.gree.jp/blog/2010/09/782/
    // JPEGTagはここに書いてある→http://svn.openpear.org/IO_SWF/branches/1.0/IO/SWF/JPEG.php
    class DefineBitsJPEG2 : DefineBits
    {
        byte[] erroneousHeader = new byte[] { 0xFF, 0xD9, 0xFF, 0xD8 };

        public DefineBitsJPEG2(XElement elm) : base(elm)
        {
        }

        protected override void initialize()
        {
            var elm = this.xelement;   
            Bitmap = decodeImage();
            ObjectID = this.xelement.Attribute("objectID").Value;
            ExtraInfo = "";
        }

        protected override Bitmap decodeImage()
        {
            var data = this.xelement.Element("data").Element("data").Value;
            var src = Convert.FromBase64String(data);

            using (var ms = new MemoryStream(src))
            {
                var br = new BinaryReader(ms);
                if (erroneousHeader.SequenceEqual(br.ReadBytes(4)))
                {
                    ExtraInfo = "erroneous header";
                    using (var ms2 = new MemoryStream())
                    {
                        ms.CopyTo(ms2);
                        return new Bitmap(ms2);
                    }
                }
            }

            return decodeJPEGSegmentedImage(src);
        }

        protected Bitmap decodeJPEGSegmentedImage(byte[] src)
        {
            using (var ms = new MemoryStream(src))
            {
                var sr = new JPEGSegmentReader(ms);
                var jpegTables = new JPEGTables(sr.ReadImage());
                var defineBits = sr.ReadImage();
                var bmp = convertToBitmap(jpegTables, defineBits);
                return bmp;
            }
        }
    }
}
