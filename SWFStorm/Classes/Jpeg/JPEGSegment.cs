﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWFStorm.Jpeg
{
    public class JPEGSegment
    {
        public JPEGSegmentMarker Marker { get; protected set; }
        public byte[] Data { get; protected set; }
        public int Length { get; protected set; }

        public JPEGSegment(JPEGSegmentMarker maker, byte[] data, int length)
        {
            Marker = maker;
            Data = data;
            Length = length;
        }

        public override string ToString()
        {
            return String.Format("{0}[{1}]", Marker, Length);
        }
    }
}
