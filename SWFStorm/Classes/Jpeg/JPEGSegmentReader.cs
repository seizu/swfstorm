﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace SWFStorm.Jpeg
{
    public class JPEGSegmentReader
    {
        public Stream BaseStream { get; protected set; }

        public JPEGSegmentReader(Stream stream)
        {
            BaseStream = stream;
        }

        /// <summary>
        /// ストリーム現在位置からJPEGセグメントを一個読み取る
        /// </summary>
        /// <returns></returns>
        public JPEGSegment Read()
        {
            var br = new BinaryReader(BaseStream);
            var marker1 = br.ReadByte();
            if (marker1 != 0xFF)
            {
                return null;
            }
            var marker2 = (JPEGSegmentMarker)br.ReadByte();
            switch (marker2)
            {
                case JPEGSegmentMarker.SOI:
                    return new JPEGSegment(marker2, null, 0);
                case JPEGSegmentMarker.EOI:
                    return new JPEGSegment(marker2, null, 0);
                case JPEGSegmentMarker.SOS:
                case JPEGSegmentMarker.RST0:
                case JPEGSegmentMarker.RST1:
                case JPEGSegmentMarker.RST2:
                case JPEGSegmentMarker.RST3:
                case JPEGSegmentMarker.RST4:
                case JPEGSegmentMarker.RST5:
                case JPEGSegmentMarker.RST6:
                case JPEGSegmentMarker.RST7:
                    var data = readScanSegment(BaseStream);
                    return new JPEGSegment(marker2, data, data.Length);
                default:
                    var length = (ushort)IPAddress.NetworkToHostOrder(br.ReadInt16());
                    return new JPEGSegment(marker2, br.ReadBytes(length - 2), length);
            }
        }

        /// <summary>
        /// ストリームの現在位置からSOIからEOIまでのJPEGセグメントを読み取る。
        /// </summary>
        /// <returns></returns>
        public List<JPEGSegment> ReadImage()
        {
            var segments = new List<JPEGSegment>();

            try
            {
                JPEGSegment segment;
                do
                {
                    segment = Read();
                } while (segment.Marker != JPEGSegmentMarker.SOI);
                
                segments.Add(segment);

                do
                {
                    segment = Read();
                    segments.Add(segment);
                } while (segment.Marker != JPEGSegmentMarker.EOI);
            }
            catch
            {
                // do nothing
            }

            return segments;
        }

        /// <summary>
        /// エスケープを含むSOSやRSTを読み取る。
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        protected byte[] readScanSegment(Stream stream)
        {
            var br = new BinaryReader(stream);
            using (var tmpStream = new MemoryStream())
            {
                byte currentByte, nextByte;
                currentByte = br.ReadByte();
                while (true)
                {
                    nextByte = br.ReadByte();
                    if (currentByte == 0xFF && nextByte != 0x00)
                    {
                        break;
                    }

                    tmpStream.WriteByte(currentByte);
                    currentByte = nextByte;
                }

                stream.Seek(-2, SeekOrigin.Current);
                return tmpStream.ToArray();
            }
        }
    }
}
